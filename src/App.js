import logo from './logo.svg';
import './App.css';

import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import { deepOrange, deepPurple } from '@mui/material/colors';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

         <Stack direction="row" spacing={2} className="mt3">
            <Avatar>H</Avatar>
            <Avatar sx={{ bgcolor: deepOrange[500] }}>N</Avatar>
            <Avatar sx={{ bgcolor: deepPurple[500] }}>OP</Avatar>
          </Stack>

          <ButtonGroup variant="contained" aria-label="outlined primary button group" className="mt3">
            <Button color="secondary">One</Button>
            <Button>Two</Button>
            <Button size="large" >Three</Button>
          </ButtonGroup>
      </header>
    </div>
  );
}

export default App;
